# Development notes -- Metrics

The goal of this part is to be able to display different metrics from Vidjil to allow better analysis of the uses of the platform. This requires several tools, to collect the metrics from the Vidjil server we use `Prometheus` and combines it with `Grafana` to display.

``` mermaid
graph LR
    A[Serveur Vidil] -->|json| B[Metrics Instance];
    B -->|query| A;
    B --> C[Prometheus];
    C --> B;
    C --> D[Grafana];
```

## Metrics instance

Metrics instance uses `Flask` to create the web application. The `main` file create the `/metrics` Flask route where is displayed the metrics scraped with prometheus. These metrics are taken from the sql database and initialized in Vidjil in the `matrics.py` file using `pydale` queries. They are then called in the Metrics server and converted into Prometheus metrics. To access the metrics, a user has to be in the metrics group, we then added a new user in the database. To log as this user when launching a new metrics instance, we stored the variables `METRICS_USER_PASSWORD` and `METRICS_USER_EMAIL` in an .env file in Metrics instance. These variables are the login keys for the metrics user and taken as parameters for the login function in the main file.


This software allow to use `API` to ask server some metrics that will be modifiy locally and exploited by `prometheus` service.

The project is a simple flask application that use vidjil API to connect as admin to a server.
The API script is embeded to vidjil/tools; include here as a submodule.


## How to use

1. Install Docker and clone this repository.
1. First, you need to build docker image:  
  ```
  docker build . --tag vidjil/metrics-app
  ```
1. Get server certificat as described in vidjil api documentation.  
  ```bash
  SERVERNAME=localhost # adapt it to your need
  echo -n | openssl s_client -connect $SERVERNAME:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > ./cert_$SERVERNAME.pem
  ```  
1. Modify some environnement values:
    * VIDJIL_USER: Admin user account to use to get metrics. Need to be created on the interogated server (metrics@vidjil.org)
    * VIDJIL_PWD: Password of the acocunt; The usage of a docker secret can be made
    * SERVICE_NAME: Name of the docker service
    * SERVER_URL: URL of server to interogate.
    * SERVER_CHAIN: Local downloaded certificate from interogated server (server chain `.pem` file)
1. Then you should just run the container:  
```
docker-compose up -d
```  
**Note:** do not forget to change port in all places(in Dockerfile, in your app and when starting container) if you are changing it.

1. After that your app should be accessible via http://127.0.0.1:5000/ and you should see "Hello World!" there:

<details> 
  ![hello world](img/hello-world.png)
  <summary>Hello world page</summary>
</details>




## Metrics getted

Metrics getted may vary from one server to another. These metrics is in Prometheus format and will be use into Grafana vidjil dashboard (see ).

Once server is launched, you should get metrics accessible at https://localhost:5000/metrics.


<details>
  ![metrics](img/metrics.png)
  <summary>Metrics page</summary>
</details>

## Metrics server

Metrics server launches the Grafana, Prometheus, BlackBox and Node exporter servers using `docker images`.

## Grafana

Once we collected metrics with prometheus, we use Grafana to display them. Grafana get launched with a docker image on the port `3000`.

username : `unicorn`
password : `UnicornExists!`

Then, head to the `Vidjil dashboard` to see the metrics scraped by prometheus. 

#### New Panel

To create a new panel to display a metric, click on `add`, `visualization`. with Prometheus as the data source you can then select a metric to display in a new query and optionally add `label filters` and/or `operations` to make it more specific. To have a better representation, you can change the graph type and modify the legend to make it mor clear.

#### New Alert

If you want to be alerted when a change (exceeds a threshold for example) happens on this new metrics, you can create a `new alert rule` in the Alert tab of your panel.
To set the alert condition of your choice, modify the `Expressions` (`Reduce` is mandatory to change the time series to a single value). Chose a folder and an evaluation group. To send a notification when the alert is firing, select a `contact point` or create a new one. You can also add other optional features to suit your needs. 

