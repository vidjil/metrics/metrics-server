# Vidjil-monitoring

This project aim to launch a monitoring infrastructure to follow mulitple vidjil instances.
You can find information on third-party-tools at: 

- [Prometheus](https://github.com/prometheus/prometheus)
- [Grafana](https://github.com/grafana/grafana)
<!-- - [Alertmanager](https://github.com/prometheus/alertmanager) -->
<!-- - [Blackbox exporter](https://github.com/prometheus/blackbox_exporter) -->
- [Node exporter](https://github.com/prometheus/node_exporter)


### Set project

#### Vidjil submodules initialisation

This project depend of vidjil component. 
A first step is needed to get vidjil reposirory fetch inside this project. 

You need to launch this command to set it up.

```
make init_submodules
```

If everything work fine, Vidjil submodule will be downloaded under `metrics/vidjil`.


#### Grafana

Default password of grafana server is `admin/admin`. 
At first login, a new password will be asked.

<!-- TODO; filled information to get correct dashboard at starting -->

#### Prometheus

Configuration should be setted in `prometheus/prometheus.yml` file.  
For vidjil metrics, everything should work out of the box. 
However, you need to declare target servers for node exporter in `measure server` job.

#### Vidjil servers

Configuration can be set in a conf file `metrics/conf.py`. A sample if present under `metrics/conf.sample.py`. 
Please copy and rename this file as `conf.py` to be sure to not send your credential on repository. 
You can after that modifiy configuration to set urln, metrics user and password

You can also use a SSL certificate for API as described in [vidjil documentation](https://www.vidjil.org/doc/api/).
On target server, you need to set up a metrics user if not already done. 
See this [page](https://www.vidjil.org/doc/admin/#server-monitoring) for more information on how to do that.

#### Node reporter

Node reporter isa dedicated service to follow hardware of a server (CPU and memory usage, disk space, ...).
It should be launch on each target and declared in prometheus as specified below.

You can use for that dedicated `docker-compose-node.yml` file. 


## Launch services

To launch this stack, you will only need to launch a docker-compose file and set some variable on server to follow :

```
docker-compose up -d 
```

Note that for the moment, no image is avaialbe for vidjil metrics.
At first lauch, you should launch `make build_metrics` to build metrics docker images that will be used. 
Note that if you made some dev and want to build again this image, you can add `--build` at `docker compsoe up -d` to build metrics server image at launch.

By default, only grafana server will be serve on port 3000.
As metrics and prometheus don't allow to manage user and credential, 
we use an internal network allowing a communication between service but not with external world.
However, you can open network by uncomment `ports` declaration of these services. 

Host network will be used. You can find services on **localhost** at these port
- Prometheus : `9090`
- Grafana : `3000`
- Metrics vidjil: `5000`
- Node exporter : `9100`
<!-- - Alertmanager : `9093` -->


<!-- Based on a projet of ouidou found on github -->
<!-- Ce dépôt est en lien avec mon article medium : https://medium.com/ouidou/un-monitoring-complet-en-quelques-minutes-avec-prometheus-33e849e6392e -->