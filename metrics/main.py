#!/usr/bin/python
# coding: utf-8
from flask import Flask, make_response
from vidjil.tools.api_vidjil import *
import os
import random
from  conf import *

app = Flask(__name__)

@app.route('/')
def entry_point():
    return 'Hello World'

def get_env_list():
    return vidjil_servers

@app.route('/metrics_test')
def metrics_test():
    content = ""
    dev_mode = os.getenv('DEV_MODE')
    
    print( "====== /metrics_test")
    # print( f"user: {user}\npwd: {password}" )
    for vidjil_server in vidjil_servers :
        url_server = vidjil_server["url"]
        certificat = vidjil_server["certificat"]
        user       = vidjil_server["user"]
        password   = vidjil_server["password"]

        print( f"=====\nurl_server: {url_server}\ncertificat: {certificat}" )
        content += f"url_server: {url_server}<br/>user: {user}<br/>certificat: {certificat}<br/>"
        if dev_mode:
            content += f"<br/>pwd: {password}"

    return content

def cleanUrlServer(url):
    return url.replace("https://", "").replace("/vidjil/", "")

def unavailableServer(url) -> str:
    return f"\
# HELP vidjil_api_request_unavailable_server {'A server is unreachable'}\n\
# TYPE vidjil_api_request_unavailable_server {'gauge'}\n\
vidjil_api_request_unavailable_server {{server='{cleanUrlServer(url)}'}}\n"
    

@app.route('/metrics')
def metrics():
    formated_response = ""

    for vidjil_server in vidjil_servers :
        url_server = vidjil_server["url"]
        certificat = vidjil_server["certificat"]
        certificat = False # For the moment, don't use it in dev mode
        user       = vidjil_server["user"]
        password   = vidjil_server["password"]

        print( "====== /metrics")
        print( f"url_server: {url_server}\ncertificat: {certificat}" )
        print( f"user: {user}\npwd: {password}" )
        try:
            vidjil = Vidjil(url_server, ssl=certificat)
            vidjil.login(user, password)

            metrics = vidjil.metrics(metrics_list="all")
            # print(f"{metrics=}")
            formated_response += formated_metrics(metrics,cleanUrlServer(url_server))
        except Exception as e:
            print( e )
            formated_response += unavailableServer(url_server)
    response = make_response(formated_response, 200)
    response.mimetype = "text/plain"
    return response


def formated_metrics(metrics, url_server):
    formater = {
        'users_count': {"type": "counter", "description": "The count of users."},
        'group_count': {"type": "counter", "description": "The count of groups."},
        'group_count_only_test': {"type": "counter", "description": "The count of groups displayed on vidjil."},

        'set_patients_count': {"type": "counter", "description": "The count of patients sets."},
        'set_runs_count': {"type": "counter", "description": "The count of runs sets."},
        'set_generic_count': {"type": "counter", "description": "The count of generic sets."},

        'results_count': {"type": "counter", "description": "The count of results."},
        'sequence_count': {"type": "counter", "description": "The count of sequences created."},
        
        'directories_sizes':    {"type": "gauge", "description": "Size of various directories.", "function": convert_directories_sizes},
        'disk_usage':     {"type": "gauge", "description": "Statistics on main disk usage.", "function": convert_disk_usage},

        'config_analysis':  { "type": "gauge", "description": "configs_analysis.", "function": convert_configs_analysis},
        'config_analysis_by_groups':  {"type": "gauge", "description": "configs_analysis.", "function": convert_configs_analysis_by_groups},
        
        'login_count':       {"type": "counter", "description": "login_count.",      "function": convert_login_count},
        'status_analysis':   {"type": "gauge", "description": "status_analysis.",  "function": convert_status_analysis},
        'sequence_by_user':   {"type": "gauge", "description": "sequence_by_user.",  "function": convert_sequence_by_user},
        'set_generics_by_user':   {"type": "gauge", "description": "set_generics_by_user.",  "function": convert_set_generics_by_user}, 
        'config_analysis_by_users_runs':   {"type": "gauge", "description": "config_analysis_by_users_runs.",  "function": convert_config_analysis_by_users_runs}, 
        'sequence_size_by_user':   {"type": "gauge", "description": "sequence_size_by_user.",  "function": convert_sequence_size_by_user}, 
        'config_analysis_by_users_generic':   {"type": "gauge", "description": "config_analysis_by_users_generic.",  "function": convert_config_analysis_by_users_generic}, 
        'set_runs_by_user':   {"type": "gauge", "description": "set_runs_by_user.",  "function": convert_set_runs_by_user}, 
        'set_generics_count':   {"type": "gauge", "description": "set_generics_count.",  "function": convert_set_generics_count}, 
        'config_analysis_by_users_patients':   {"type": "gauge", "description": "config_analysis_by_users_patients.",  "function": convert_config_analysis_by_users_patients}, 
        'set_patients_by_user':   {"type": "gauge", "description": "set_patients_by_user.",  "function": convert_set_patients_by_user},


        # Long - {'set_generics_by_group', 'request_times', 'set_runs_by_group', 'message', 'set_patients_by_group'}
        'set_generics_by_group':   {"type": "gauge", "description": "set_generics_by_group.",  "function": convert_set_generics_by_group},
        'set_runs_by_group':   {"type": "gauge", "description": "set_runs_by_group.",  "function": convert_set_runs_by_group},
        'set_patients_by_group':   {"type": "gauge", "description": "set_patients_by_group.",  "function": convert_set_patients_by_group},

        ### Not used
        # 'request_times':   {"type": "gauge", "description": "request_times.",  "function": convert_request_times}, 
    }

    string = f"# Information : {metrics['message']}\n"
    not_found_metrics = "# ====== Not found metrics ======\n"

    for metrics_key in formater.keys():
        if metrics_key in metrics.keys():
            if 'function' in formater[metrics_key].keys():
                string += formater[metrics_key]["function"](metrics_key, formater[metrics_key], metrics[metrics_key], url_server)
            else:
                string += f"\
# HELP vidjil_api_request_{metrics_key} {formater[metrics_key]['description']}\n\
# TYPE vidjil_api_request_{metrics_key} {formater[metrics_key]['type']}\n\
vidjil_api_request_{metrics_key}{{server=\"{url_server}\"}} {metrics[metrics_key]}\n"
        else:
                not_found_metrics += f"# KEY NOT FOUND: {metrics_key}\n"

    string += not_found_metrics

    not_converted = metrics.keys() - formater.keys()
    string += f"# Not converted: {not_converted}\n"
    return string

def convert_directories_sizes(metrics_key, formater, data, url_server):
    # {"size": size_directory("/mnt/result/results"), "path": "/mnt/result/results", "name": "results"}
    string = f"\
# HELP vidjil_api_request_directories_sizes {formater['description']}\n\
# TYPE vidjil_api_request_directories_sizes {formater['type']}\n"
    for elt in data:
        string += f"\
vidjil_api_request_directories_sizes\
{{name=\"{elt['name']}\", path=\"{elt['path']}\", server=\"{url_server}\"}} \
{elt['size']}\n"
    return string

def convert_disk_usage(metrics_key, formater, data, url_server):
    # {"name": "total", "size": xxx}
    string = f"\
# HELP vidjil_api_request_disk_usage {formater['description']}\n\
# TYPE vidjil_api_request_disk_usage {formater['type']}\n"
    for elt in data:
        string += f"\
vidjil_api_request_disk_usage\
{{name=\"{elt['name']}\", server=\"{url_server}\"}} \
{elt['size']}\n"
    return string

def convert_configs_analysis(metrics_key, formater, data, url_server):
    # "configs_analysis": [{
    #       "_extra": {"COUNT(`results_file`.`id`)": 12 },
    #       "config": {"name": "multi+inc+xxx", "program": "vidjil"},
    #       "results_file": {"config_id": 2 }
    #     }, ... ],
    
    # print(data)
    string = f"\
# HELP vidjil_api_request_config_analysis {formater['description']}\n\
# TYPE vidjil_api_request_config_analysis {formater['type']}\n"
    for elt in data:
        string += f"\
vidjil_api_request_config_analysis\
{{config_name=\"{elt['config']['name'].replace('+','_')}\", config_program=\"{elt['config']['program']}\", config_id=\"{elt['results_file']['config_id']}\", server=\"{url_server}\"}} \
{elt['_extra']['COUNT(`results_file`.`id`)']}\n"
    return string

def convert_configs_analysis_by_groups(metrics_key, formater, data, url_server):
    # "configs_analysis": [{
    #       "_extra": {"COUNT(`results_file`.`id`)": 12 },
    #       "config": {"name": "multi+inc+xxx", "program": "vidjil"},
    #       "results_file": {"config_id": 2 }
    #     }, ... ],
    string = f"\
# HELP vidjil_api_request_config_analysis_by_groups {formater['description']}\n\
# TYPE vidjil_api_request_config_analysis_by_groups {formater['type']}\n"
    for elt in data:
        string += f"\
vidjil_api_request_config_analysis_by_groups\
{{config_name=\"{elt['config']['name'].replace('+','_')}\", config_program=\"{elt['config']['program']}\", config_id=\"{elt['results_file']['config_id']}\", group=\"{elt['auth_group']['role']}\", server=\"{url_server}\"}} \
{elt['_extra']['COUNT(`results_file`.`id`)']}\n"
    return string

def convert_login_count(metrics_key, formater, data, url_server):
    #   "login_count": [{
    #       "_extra": {"COUNT(`auth_event`.`id`)": 14 },
    #       "auth_event": {"user_id": 1 },
    #       "auth_user": {"email": "plop@plop.com"}
    #     }, ... ]
    
    # print(data)
    string = f"\
# HELP vidjil_api_request_login_count {formater['description']}\n\
# TYPE vidjil_api_request_login_count {formater['type']}\n"
    for elt in data:
        if elt['auth_user']['email'] == "metrics@vidjil.org":
            continue
        string += f"\
vidjil_api_request_login_count\
{{user_id=\"{elt['auth_event']['user_id']}\", user_email=\"{elt['auth_user']['email']}\", server=\"{url_server}\"}} \
{elt['_extra']['COUNT(`auth_event`.`id`)']}\n"
    return string


def convert_status_analysis(metrics_key, formater, data, url_server):
    #   "status_analysis": [{
    #       "_extra": {"COUNT(`scheduler_task`.`status`)": 6 },
    #       "scheduler_task": {"status": "COMPLETED", "task_name": "process"}
    #     }, ...]
    # print(data)
    string = f"\
# HELP vidjil_api_request_status_analysis {formater['description']}\n\
# TYPE vidjil_api_request_status_analysis {formater['type']}\n"
    for elt in data:
        string += f"\
vidjil_api_request_status_analysis\
{{status=\"{elt['scheduler_task']['status']}\", task_name=\"{elt['scheduler_task']['task_name']}\", server=\"{url_server}\"}} \
{elt['_extra']['COUNT(`scheduler_task`.`id`)']}\n"
    return string




def convert_set_generics_by_group(metrics_key, formater, data, url_server):
    # return f"\n\n=*=*= set_generics_by_group ==> {data[:2]}\n\n\n" # 'count': 1, 'group_id': 1, 'group_name': 'admin'
    string = f"\
# HELP vidjil_api_request_set_generics_by_group {formater['description']}\n\
# TYPE vidjil_api_request_set_generics_by_group {formater['type']}\n"
    for elt in data:
            string += f"\
vidjil_api_request_set_generics_by_group\
{{group_id=\"{elt['group_id']}\", group_name=\"{elt['group_name']}\", server=\"{url_server}\"}} {elt['count']}\n"
    return string


def convert_set_runs_by_group(metrics_key, formater, data, url_server):
    # return f"\n\n=*=*= set_runs_by_group ==> {data[:2]}\n\n\n" #
    string = f"\
# HELP vidjil_api_request_set_runs_by_group {formater['description']}\n\
# TYPE vidjil_api_request_set_runs_by_group {formater['type']}\n"
    for elt in data:
            string += f"\
vidjil_api_request_set_runs_by_group\
{{group_id=\"{elt['group_id']}\", group_name=\"{elt['group_name']}\", server=\"{url_server}\"}} {elt['count']}\n"
    return string


def convert_set_patients_by_group(metrics_key, formater, data, url_server):
    # return f"\n\n=*=*= set_patients_by_group ==> {data[:2]}\n\n\n" #
    string = f"\
# HELP vidjil_api_request_set_patients_by_group {formater['description']}\n\
# TYPE vidjil_api_request_set_patients_by_group {formater['type']}\n"
    for elt in data:
            string += f"\
vidjil_api_request_set_patients_by_group\
{{group_id=\"{elt['group_id']}\", group_name=\"{elt['group_name']}\", server=\"{url_server}\"}} {elt['count']}\n"
    return string


def convert_sequence_by_user(metrics_key, formater, data, url_server):
    # return f"\n\n=*=*= convert_sequence_by_user ==> {data[:2]}\n\n\n" #
    string = f"\
# HELP vidjil_api_request_sequence_by_user {formater['description']}\n\
# TYPE vidjil_api_request_sequence_by_user {formater['type']}\n"
    for elt in data:
            string += f"\
vidjil_api_request_sequence_by_user\
{{user_id=\"{elt['user_id']}\", server=\"{url_server}\"}} {elt['count_sequence']}\n"
    return string


def convert_set_generics_by_user(metrics_key, formater, data, url_server):
    # return f"=*=*= convert_set_generics_by_user ==> {data[:2]}" #  'count': 1, 'user_id': 1
    string = f"\
# HELP vidjil_api_request_set_generics_by_user {formater['description']}\n\
# TYPE vidjil_api_request_set_generics_by_user {formater['type']}\n"
    for elt in data:
            string += f"\
vidjil_api_request_set_generics_by_user\
{{user_id=\"{elt['user_id']}\", server=\"{url_server}\"}} {elt['count']}\n"
    return string


def convert_config_analysis_by_users_runs(metrics_key, formater, data, url_server):
    # return f"convert_config_analysis_by_users_runs ==> {data[:2]}"
    # 'config_id': 2, 'config_name': 'multi+inc+xxx', 'count': 1, 'user_id': 1
    string = f"\
# HELP vidjil_api_request_config_analysis_by_users_runs {formater['description']}\n\
# TYPE vidjil_api_request_config_analysis_by_users_runs {formater['type']}\n"
    for elt in data:
            string += f"\
vidjil_api_request_config_analysis_by_users_runs\
{{user_id=\"{elt['user_id']}\", config_id=\"{elt['config_id']}\", config_name=\"{elt['config_name']}\", server=\"{url_server}\"}} {elt['count']}\n"
    return string


def convert_sequence_size_by_user(metrics_key, formater, data, url_server):
    string = f"\
# HELP vidjil_api_request_sequence_size_by_user {formater['description']}\n\
# TYPE vidjil_api_request_sequence_size_by_user {formater['type']}\n"
    for elt in data:
            # Some old data have none value (no provider filled); needto replace None by 0
            size = elt['size_file_sum'] if not elt['size_file_sum'] == None else -1
            string += f"\
vidjil_api_request_sequence_size_by_user\
{{user_id=\"{elt['user_id']}\", server=\"{url_server}\"}} {size}\n"
    return string


def convert_config_analysis_by_users_generic(metrics_key, formater, data, url_server):
    #   "status_analysis": [{
    #       'config_id': 9, 'config_name': 'spikes', 'count': 4, 'user_id': 1
    #     }, ...]
    # print( "=====\n===== convert_config_analysis_by_users_generic")
    # print(f"{data[:2]=}")
    string = f"\
# HELP vidjil_api_request_config_analysis_by_users_generic {formater['description']}\n\
# TYPE vidjil_api_request_config_analysis_by_users_generic {formater['type']}\n"
    for elt in data:
            string += f"\
vidjil_api_request_config_analysis_by_users_generic\
{{config_id=\"{elt['config_id']}\", config_name=\"{elt['config_name']}\", user_id=\"{elt['user_id']}\", server=\"{url_server}\"}} {elt['count']}\n"
    return string


def convert_set_runs_by_user(metrics_key, formater, data, url_server):
    # "status_analysis": [{
    #       '_extra': {'COUNT(`run`.`id`) AS count': 2, '`run`.`creator` AS user_id': 1}, 
    #       'count': 2, 
    #       'user_id': 1
    #   }]
    print( "convert_set_runs_by_user")
    print(data[:2])
    string = f"\
# HELP vidjil_api_request_set_runs_by_user {formater['description']}\n\
# TYPE vidjil_api_request_set_runs_by_user {formater['type']}\n"
    for elt in data:
            string += f"\
vidjil_api_request_set_runs_by_user\
{{user_id=\"{elt['user_id']}\", server=\"{url_server}\"}} {elt['count']}\n"
    return string


def convert_set_generics_count(metrics_key, formater, data, url_server):
    print("convert_set_generics_count")
    print(f"{data=}")
    string = f"\
# HELP vidjil_api_request_set_generics_count {formater['description']}\n\
# TYPE vidjil_api_request_set_generics_count {formater['type']}\n"
    string += f"\
vidjil_api_request_set_generics_count\
{{server=\"{url_server}\"}} {data} \n"
    return string


# def convert_request_times(metrics_key, formater, data, url_server):
#     return f"=*=*= convert_request_times ==> {data}"
#     string = f"\
# # HELP vidjil_api_request_request_times {formater['description']}\n\
# # TYPE vidjil_api_request_request_times {formater['type']}\n"
#     return string


def convert_config_analysis_by_users_patients(metrics_key, formater, data, url_server):
    # return f"=*=*= convert_config_analysis_by_users_patients ==> {data[:2]}"
    string = f"\
# HELP vidjil_api_request_config_analysis_by_users_patients {formater['description']}\n\
# TYPE vidjil_api_request_config_analysis_by_users_patients {formater['type']}\n"
    for elt in data:
            string += f"\
vidjil_api_request_config_analysis_by_users_patients\
{{config_id=\"{elt['config_id']}\", config_name=\"{elt['config_name']}\", user_id=\"{elt['user_id']}\", server=\"{url_server}\"}} {elt['count']}\n"
    return string


def convert_set_patients_by_user(metrics_key, formater, data, url_server):
    #   "status_analysis": [{
    #     '_extra': {'COUNT(`patient`.`id`) AS count': 49, '`patient`.`creator` AS user_id': 14}, 
    #     'count': 49, 
    #     'user_id': 14
    # }, ...]
    string = f"\
# HELP vidjil_api_request_set_patients_by_user {formater['description']}\n\
# TYPE vidjil_api_request_set_patients_by_user {formater['type']}\n"
    for elt in data:
            string += f"\
vidjil_api_request_set_patients_by_user\
{{user_id=\"{elt['user_id']}\", server=\"{url_server}\"}} {elt['count']}\n"
    return string



# def convert_sequence_by_user(metrics_key, formater, data):
#     #   "login_count": [{
#     #       "_extra": {"COUNT(`auth_event`.`id`)": 14 },
#     #       "auth_event": {"user_id": 1 },
#     #       "auth_user": {"email": "plop@plop.com"}
#     #     }, ... ]
    
#     print(data)
#     string = f"\
# # HELP vidjil_api_sequence_by_user {formater['description']}\n\
# # TYPE vidjil_api_sequence_by_user {formater['type']}\n"
#     for elt in data:
#         print(elt)
#         if elt['auth_user']['email'] == "metrics@vidjil.org":
#             continue
#         string += f"\
# vidjil_api_sequence_by_user\
# {{user_id=\"{elt['sequence_file']['user_id']}\"}} \
# {elt['_extra']['count_sequence(`sequence_file`.`id`)']}\n"
#     return string

# def get_file_size(file_path):
#     return os.path.getsize(file_path)

# def convert_size_file(metrics_key, formater, data):
#     #   "status_analysis": [{
#     #       "_extra": {"COUNT(`scheduler_task`.`status`)": 6 },
#     #       "scheduler_task": {"status": "COMPLETED", "task_name": "process"}
#     #     }, ...]
#     string = f"\
# # HELP vidjil_api_request_size_file {formater['description']}\n\
# # TYPE vidjil_api_request_size_file {formater['type']}\n"
#     for elt in data:
#         size = get_file_size(elt['sequence_file']['data_file'])
#         string += f"\
# vidjil_api_request_size_file\
# {{file_name=\"{elt['sequence_file']['data_file']}\"}} \
# {size}\n"
#     return string

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)


